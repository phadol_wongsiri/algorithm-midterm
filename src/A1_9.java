import java.util.Scanner;

public class A1_9 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String s = kb.nextLine();
        System.out.println(ParseInt(s));
    }

    static int ParseInt(String s) {
        int sum = 0;
        int len = s.length();
        for (int i = 0; i < len; i++) {
            int num = 0;
            switch (s.charAt(i)) {
                case '0':
                    break;

                case '1':
                    num = 1;
                    break;

                case '2':
                    num = 2;
                    break;

                case '3':
                    num = 3;
                    break;

                case '4':
                    num = 4;
                    break;

                case '5':
                    num = 5;
                    break;

                case '6':
                    num = 6;
                    break;

                case '7':
                    num = 7;
                    break;

                case '8':
                    num = 8;
                    break;

                case '9':
                    num = 9;
                    break;

                default:
                    break;
            }
            sum += num * Math.pow(10, len - 1 - i);
        }
        return sum;
    }
}