import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class A1_10 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String s = kb.nextLine();
        ArrayList<String> strarray = new ArrayList<>(Arrays.asList(s.split(" ")));
        ArrayList<Integer> array = new ArrayList<>();
        for (String str : strarray) {
            array.add(Integer.parseInt(str));
        }
        System.out.println(LongestSubarray(array));
    }

    static ArrayList<Integer> LongestSubarray(ArrayList<Integer> A) {
        ArrayList<Integer> longest = new ArrayList<>();
        ArrayList<Integer> tmp = new ArrayList<>();
        for (Integer num : A) {
            if (tmp.isEmpty()) {
                tmp.add(num);
                if (longest.isEmpty()) {
                    longest = new ArrayList<>(tmp);
                }
            } else {
                if (num >= tmp.get(tmp.size() - 1)) {
                    tmp.add(num);
                } else {
                    tmp = new ArrayList<>();
                    tmp.add(num);
                }
                if (tmp.size() > longest.size()) {
                    longest = new ArrayList<>(tmp);
                }
            }
        }
        return longest;
    }
}